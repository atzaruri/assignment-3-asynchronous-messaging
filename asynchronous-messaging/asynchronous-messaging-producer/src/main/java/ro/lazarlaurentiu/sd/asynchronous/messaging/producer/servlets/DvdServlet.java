package ro.lazarlaurentiu.sd.asynchronous.messaging.producer.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeoutException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.SerializationUtils;

import com.google.gson.JsonObject;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import ro.lazarlaurentiu.sd.asynchronous.messaging.models.Dvd;

/**
 * Servlet implementation class DvdServlet
 */
public class DvdServlet extends HttpServlet {

	private static final String QUEUE_NAME = "DvdQueue";
	private static final String QUEUE_HOST = "localhost";
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DvdServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("dvd.html");
		requestDispatcher.forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String title = (String) request.getParameter("title");
		int year = Integer.parseInt((String) request.getParameter("year"));
		double price = Double.parseDouble((String) request.getParameter("price"));

		Dvd dvd = new Dvd(title, year, price);

		JsonObject jsonObject = new JsonObject();

		PrintWriter out = response.getWriter();
		response.setContentType("text/html");

		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost(QUEUE_HOST);
		Connection connection = null;
		Channel channel = null;
		try {
			connection = connectionFactory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			channel.basicPublish("", QUEUE_NAME, null, SerializationUtils.serialize(dvd));
		} catch (TimeoutException e) {
			e.printStackTrace();

			jsonObject.addProperty("success", false);
			jsonObject.addProperty("message", "Dvd posting failed!");
			out.println(jsonObject.toString());
			out.close();

			return;
		}

		jsonObject.addProperty("success", true);
		jsonObject.addProperty("message", "Dvd posting successful!");
		out.println(jsonObject.toString());
		out.close();
	}

}
