$(document).ready(function() {
	
	$("#dvdForm").submit(function(e) {
		e.preventDefault();
	});
	
	$("#submitButton").click(function(e) {
		dataString = $("#dvdForm").serialize();
		
		$.ajax({
			type: "POST",
			url: "/asynchronous-messaging-producer/dvd",
			data: dataString,
			dataType: "json",
			
			success: function(data, textStatus, jqXHR) {
				$("#message").html(data.message);
			},
			
			error: function(jqXHR, textStatus, errorThrown) {
				$("#message").html("An error occurred during post request. No response from server.");
			}
		});
	});
	
});