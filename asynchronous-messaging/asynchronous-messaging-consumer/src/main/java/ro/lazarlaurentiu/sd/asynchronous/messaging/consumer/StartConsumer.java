package ro.lazarlaurentiu.sd.asynchronous.messaging.consumer;

import ro.lazarlaurentiu.sd.asynchronous.messaging.consumer.services.ConsoleService;
import ro.lazarlaurentiu.sd.asynchronous.messaging.consumer.services.FileService;
import ro.lazarlaurentiu.sd.asynchronous.messaging.consumer.services.MailService;
import ro.lazarlaurentiu.sd.asynchronous.messaging.consumer.services.ServiceRegistry;

public class StartConsumer {

	public static void main(String[] args) {
		// Register services to server
		ServiceRegistry.registerService("ConsoleService", new ConsoleService());
		ServiceRegistry.registerService("FileService", new FileService());
		ServiceRegistry.registerService("MailService",
				new MailService("projects.lazar.laurentiu@gmail.com", "Projects.p4ss"));

		try {
			new Consumer();
		} catch (Exception e) {
			System.out.println("Failed to start consumer!");
			e.printStackTrace();
		}
	}

}
