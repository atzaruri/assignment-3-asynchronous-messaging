package ro.lazarlaurentiu.sd.asynchronous.messaging.consumer.services;

import ro.lazarlaurentiu.sd.asynchronous.messaging.models.Dvd;

public class ConsoleService implements IService {

	public boolean processDvd(Dvd dvd) {
		System.out.println("(ConsoleService)Processed: " + dvd.toString());
		return true;
	}

}
