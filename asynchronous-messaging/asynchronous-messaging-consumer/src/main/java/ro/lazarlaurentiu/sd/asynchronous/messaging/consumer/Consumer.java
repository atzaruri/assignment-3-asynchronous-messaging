package ro.lazarlaurentiu.sd.asynchronous.messaging.consumer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import ro.lazarlaurentiu.sd.asynchronous.messaging.consumer.services.IService;
import ro.lazarlaurentiu.sd.asynchronous.messaging.consumer.services.ServiceRegistry;
import ro.lazarlaurentiu.sd.asynchronous.messaging.models.Dvd;

public class Consumer {

	private static final String QUEUE_NAME = "DvdQueue";
	private static final String QUEUE_HOST = "localhost";

	public Consumer() throws IOException, TimeoutException {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost(QUEUE_HOST);
		Connection connection = connectionFactory.newConnection();
		Channel channel = connection.createChannel();

		channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		channel.basicConsume(QUEUE_NAME, true, new DvdConsumer(channel));

		System.out.println("Consumer started successfully!");
	}

	private class DvdConsumer extends DefaultConsumer {

		public DvdConsumer(Channel channel) {
			super(channel);
		}

		@Override
		public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
				throws IOException {
			Dvd dvd = (Dvd) SerializationUtils.deserialize(body);
			for (IService service : ServiceRegistry.getAllRegisteredServices()) {
				service.processDvd(dvd);
			}
		}
	}
}
