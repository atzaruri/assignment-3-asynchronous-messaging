package ro.lazarlaurentiu.sd.asynchronous.messaging.consumer.services;

import ro.lazarlaurentiu.sd.asynchronous.messaging.models.Dvd;

public interface IService {

	public boolean processDvd(Dvd dvd);

}
