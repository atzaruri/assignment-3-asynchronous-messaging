package ro.lazarlaurentiu.sd.asynchronous.messaging.consumer.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import ro.lazarlaurentiu.sd.asynchronous.messaging.models.Dvd;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description: Provides method for sending an e-mail to a desired address. By
 *               default, it uses the GMail SMTP. If desired, change the the
 *               properties in the constructor.
 */
public class MailService implements IService {

	final String username;
	final String password;
	final Properties props;

	/**
	 * Builds a mail service class, used for sending e-mails. The credentials
	 * provided should be the ones needed to autenthicate to the SMTP server
	 * (GMail by default).
	 *
	 * @param username
	 *            username to log in to the smtp server
	 * @param password
	 *            password to log in to the smtp server
	 */
	public MailService(String username, String password) {
		this.username = username;
		this.password = password;

		props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
	}

	/**
	 * Sends an email with the subject and content specified, to the address
	 * specified.
	 *
	 * @param to
	 *            address to send email to
	 * @param subject
	 *            subject of the email
	 * @param content
	 *            content of the email
	 */
	public boolean sendMail(String to, String subject, String content) {
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(content);

			Transport.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("(MailService) Failed send email to: " + to);
			return false;
		}

		System.out.println("(MailService) Mail send to: " + to);
		return true;
	}

	public boolean processDvd(Dvd dvd) {
		BufferedReader input = null;

		try {
			input = new BufferedReader(new FileReader(new File("mailSubscribers.txt")));
			String line = input.readLine();
			while (line != null) {
				sendMail(line, "New DVD: " + dvd.getTitle(), dvd.toString());
				line = input.readLine();
			}

		} catch (FileNotFoundException e) {
			System.out.println("(MailService) Failed to find subscriber file!");
			return false;
		} catch (IOException e) {
			System.out.println("(MailService) I/O Exception!");
			e.printStackTrace();
			return false;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					// ignore this exception
					e.printStackTrace();
				}
			}
		}
		return true;
	}

}
