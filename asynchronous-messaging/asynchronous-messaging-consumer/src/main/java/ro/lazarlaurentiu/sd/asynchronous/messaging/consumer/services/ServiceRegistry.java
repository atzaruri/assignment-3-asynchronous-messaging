package ro.lazarlaurentiu.sd.asynchronous.messaging.consumer.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ServiceRegistry {

	private static Map<String, IService> services = new TreeMap<String, IService>();

	public static void registerService(String identification, IService service) {
		services.put(identification, service);
	}

	public static List<IService> getAllRegisteredServices() {
		return new ArrayList<IService>(services.values());
	}

}
