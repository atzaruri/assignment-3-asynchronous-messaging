package ro.lazarlaurentiu.sd.asynchronous.messaging.consumer.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import ro.lazarlaurentiu.sd.asynchronous.messaging.models.Dvd;

public class FileService implements IService {

	public boolean processDvd(Dvd dvd) {
		BufferedWriter output = null;
		File file = new File(dvd.getTitle() + ".txt");

		try {
			output = new BufferedWriter(new FileWriter(file));
			output.write(dvd.toString());
			System.out.println("(FileService) File created: " + file.getName());
		} catch (IOException e) {
			System.out.println("(FileService) File creation FAILED: " + file.getName());
			return false;
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					System.out.println("(FileService) File creation FAILED: " + file.getName());
					return false;
				}
			}
		}

		return true;
	}

}
