package ro.tuc.dsrl.ds.handson.assig.three.consumer.file.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import ro.tuc.dsrl.ds.handson.assig.three.models.Dvd;

public class FileService {

	public boolean saveDvdToFile(Dvd dvd) {
		BufferedWriter output = null;

		try {
			File file = new File(dvd.getTitle() + ".txt");
			output = new BufferedWriter(new FileWriter(file));
			output.write(dvd.toString());
		} catch (IOException e) {
			return false;
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					return false;
				}
			}
		}
		return true;
	}

}
