package ro.tuc.dsrl.ds.handson.assig.three.consumer.file.start;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.file.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.file.service.FileService;
import ro.tuc.dsrl.ds.handson.assig.three.models.Dvd;

import java.io.IOException;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description: Starting point for the Consumer Client application. This
 *               application will run in an infinite loop and retrieve messages
 *               from the queue server and send e-mails with them as they come.
 */
public class ClientStart {

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost", 8888);
		FileService fileService = new FileService();

		while (true) {
			try {
				Dvd dvd = queue.readMessage();
				if (fileService.saveDvdToFile(dvd)) {
					System.out.println("File: " + dvd.getTitle() + ".txt created with success!");
				} else {
					System.out.println("Failed to create file: " + dvd.getTitle() + ".txt!");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
