package ro.tuc.dsrl.ds.handson.assig.three.models;

import java.io.Serializable;

public class Dvd implements Serializable {

	private String title;
	private int year;
	private double price;

	private static final long serialVersionUID = 1L;

	public Dvd(String title, int year, double price) {
		super();
		this.title = title;
		this.year = year;
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String toString() {
		return "DVD - Title: " + title + ", Year: " + year + ", Price: " + price;
	}

}
